//
//  Optional+Extensions.swift
//  CheckMyPixel
//
//  Created by Gavrysh on 2/17/18.
//  Copyright © 2018 Stanislav Cherkasov. All rights reserved.
//

extension Optional {
    func apply<Result>(_ transform: ((Wrapped) -> Result)?) -> Result? {
        return self.flatMap { value in
            transform.map { $0(value) }
        }
    }
    
    public func apply<Value, Result>(_ value: Value?) -> Result?
        where Wrapped == (Value) -> Result
    {
        // ERROR: swift 4.0 too stupid, check back laterback
        //return value.apply(self)
        return self.flatMap { value.map($0) }
    }
    
    public func flatten<Result>() -> Result? where Wrapped == Result? {
        return self.flatMap { $0 }
    }
    
    public func `do`(_ action: (Wrapped) -> ()) {
        self.map(action)
    }
}

func map<Value, Result>(_ value: Value?, transform: (Value) -> Result) -> Result? {
    switch value {
    case let .some(value):
        return transform(value)
    case Optional<Value>.none:
        return nil
    }
}

precedencegroup CompositionPrecedence {
    associativity: left
    higherThan: AssignmentPrecedence
}

infix operator ●: CompositionPrecedence

func compose<A, B, C>(_ f: @escaping (A) -> B, _ g: @escaping (B) -> C) -> (A) -> C {
    return { g(f($0)) }
}

func ●<A, B, C>(_ f: @escaping (A) -> B, _ g: @escaping (B) -> C) -> (A) -> C {
    return compose(f, g)
}

precedencegroup MonadCompositionPrecedence {
    associativity: left
    higherThan: DefaultPrecedence
}

infix operator >>->>: MonadCompositionPrecedence

func mcompose<A, B, C>(_ f: @escaping (A) -> B?, _ g: @escaping (B) -> C?) -> (A) -> C? {
    return { f($0).flatMap(g) }
}

func >>->><A, B, C>(_ f: @escaping (A) -> B?, _ g: @escaping (B) -> C?) -> (A) -> C? {
    return mcompose(f, g)
}

func apply<Value, Result>(_ value: Value?, _ transform: ((Value) -> Result)?) -> Result? {
    return value.flatMap { x in
        transform.map { $0(x) }
    }
}


precedencegroup ApplicativeCompositionPrecedence {
    associativity: left
    higherThan: DefaultPrecedence
}

/*
infix operator >>>: ApplicativeCompositionPrecedence

func acompose<A, B, C>(_ f: ((A) -> B), _ g: ((B) -> C)?) -> (A) -> C? {
    return {
        f($0).flatMap { x in
            g.map { $0(x) }
        }
    }
}

func >>><A, B, C>(_ f: (A) -> B, _ g:  ((B) -> C)?) -> (A) -> C? {
    return acompose(f, g)
}
*/
 
func flatten<Value>(_ x: Value??) -> Value? {
    return x.flatMap { $0 }
}
