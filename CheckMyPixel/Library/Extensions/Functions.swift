//
//  Functions.swift
//  CheckMyPixel
//
//  Created by Gavrysh on 2/19/18.
//  Copyright © 2018 Stanislav Cherkasov. All rights reserved.
//

import Foundation

func cast<Value, Result>(_ value: Value) -> Result? {
    return value as? Result
}
