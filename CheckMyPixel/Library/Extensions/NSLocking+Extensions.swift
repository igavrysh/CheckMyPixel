//
//  NSLocking+Extensions.swift
//  CheckMyPixel
//
//  Created by Gavrysh on 2/20/18.
//  Copyright © 2018 Stanislav Cherkasov. All rights reserved.
//

import Foundation

let lock = NSLock()
var i = 1

let async: (@escaping @convention(block) () -> Swift.Void) -> () = {
    DispatchQueue.global(qos: .background).async(execute: $0)
}

public extension NSLocking {
    public func `do`<Result>(action: () -> Result) -> Result {
        self.lock()
        defer { self.unlock() }
        
        return action()
    }
}
