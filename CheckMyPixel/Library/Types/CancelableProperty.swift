//
//  CancelableProperty.swift
//  CheckMyPixel
//
//  Created by Gavrysh on 2/20/18.
//  Copyright © 2018 Stanislav Cherkasov. All rights reserved.
//

import Foundation

public protocol Cancelable {
    func cancel()
}

public class CancelableProperty {
    // MARK: -
    // MARK: Properties
    
    public var value: Cancelable? {
        willSet {
            value?.cancel()
        }
    }
}
