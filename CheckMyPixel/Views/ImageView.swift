//
//  ImageView.swift
//  CheckMyPixel
//
//  Created by Gavrysh on 2/20/18.
//  Copyright © 2018 Stanislav Cherkasov. All rights reserved.
//

import Foundation
import UIKit

// TODO: Use SnapKit

public class ImageView: UIView, Reusable {
    
    // MARK: -
    // MARK: Properties
    
    public let contentImageView = UIImageView()
    public var model: ImageModel? {
        didSet {
            self.fill(with: model)
        }
    }
    
    private var cancelableProperty = CancelableProperty()
    
    // MARK: -
    // MARK: Init and Deinit
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setup()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setup()
    }
    
    // MARK: -
    // MARK: Open
    
    
    /// Prepared subviews. This method is called during init
    /// - warning: This is an override poin. Never call it directly
    open func setup() {
        let imageView = self.contentImageView
        self.addSubview(imageView)
        imageView.frame = self.bounds
        
        imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imageView.contentMode = .scaleAspectFill
    }
    
    open func prepareForReuse() {
        self.contentImageView.image = nil
        self.model = nil
    }
    
    // MARK: -
    // MARK: Private
    
    private func fill(with model: ImageModel?) {
        self.cancelableProperty.value = model?.load { [weak self] image in
            DispatchQueue.main.async {
                if lift(self?.model, model).map( == ) ?? false {
                    self?.contentImageView.image = image
                }
            }
        }
    }
}


