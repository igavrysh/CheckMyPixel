//
//  UserCell.swift
//  CheckMyPixel
//
//  Created by Gavrysh on 2/20/18.
//  Copyright © 2018 Stanislav Cherkasov. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {
    
    // MARK: -
    // MAKR: Properties
    
    @IBOutlet private(set) var nameLabel: UILabel?
    @IBOutlet private(set) var photoImageView: ImageView?
    
    var user: User? {
        didSet {
            self.fill(with: user)
        }
    }
    
    private let task = CancelableProperty()
    
    // MARK: -
    // MARK: Open
    
    open func fill(with model: User?) {
        self.nameLabel?.text = self.user?.name
        self.user.do {
            self.fillPhoto(with: $0)
        }
    }
    
    open override func prepareForReuse() {
        super.prepareForReuse()
        
        self.nameLabel?.text = nil
        self.photoImageView?.prepareForReuse()
    }
    
    // MARK: -
    // MARK: Private
    
    private func fillPhoto(with user: User) {
        let imageLoadService = ImageLoadServiceImpl.init(
            networkService: NetworkServiceImpl(session: URLSession(configuration: .default))
        )
        
        self.photoImageView?.model = (user.image?.urlData?.url)
            .flatMap(URL.init(string:))
            .map { ImageModelImpl.init(with: $0, imageLoadService: imageLoadService) }
    }
}
