//
//  Reusable.swift
//  CheckMyPixel
//
//  Created by Gavrysh on 2/22/18.
//  Copyright © 2018 Stanislav Cherkasov. All rights reserved.
//

import UIKit

public protocol Reusable {
    func prepareForReuse()
}

extension UITableViewCell: Reusable {}
extension UICollectionViewCell: Reusable {}
