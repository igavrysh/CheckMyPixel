//
//  ImageDownloadService.swift
//  CheckMyPixel
//
//  Created by Gavrysh on 2/19/18.
//  Copyright © 2018 Stanislav Cherkasov. All rights reserved.
//

import Foundation
import UIKit

public protocol ImageLoadService {
    func fetchImage(url: URL, completion: @escaping (UIImage?) -> ()) -> Cancelable
}
