//
//  ImageDownloadServiceImpl.swift
//  CheckMyPixel
//
//  Created by Gavrysh on 2/19/18.
//  Copyright © 2018 Stanislav Cherkasov. All rights reserved.
//

import Foundation
import UIKit

class ImageLoadServiceImpl: ImageLoadService {
    
    // MARK: -
    // MARK: Properties
    
    public let networkService: NetworkService
    
    // MARK: -
    // MARK: Init and Deinit
    
    public init(networkService: NetworkService) {
        self.networkService = networkService
    }
    
    // MARK: -
    // MARK: Open
    
    open func fetchImage(url: URL, completion: @escaping (UIImage?) -> ()) -> Cancelable {
        return self.networkService.data(at: url) { data, error in
            if error != nil {
                completion(nil)
            } else {
                completion(data.flatMap(UIImage.init(data:)))
            }
        }
    }
}
