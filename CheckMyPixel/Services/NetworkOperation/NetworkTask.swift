//
//  NetworkTask.swift
//  CheckMyPixel
//
//  Created by Gavrysh on 2/19/18.
//  Copyright © 2018 Stanislav Cherkasov. All rights reserved.
//

import Foundation

public class NetworkTask: Cancelable {
    
    // MARK: -
    // MARK: Properties
    
    public private(set) var isCanceled: Bool = false
    
    private weak var urlSessionTask: URLSessionTask?
    
    // MARK: -
    // MARK: Init an Deinitan
    
    public init(urlSessionTask: URLSessionTask) {
        self.urlSessionTask = urlSessionTask
    }
    
    // MARK: -
    // MARK: PublicdataTask
    
    public func cancel() {
        self.urlSessionTask?.cancel()
        self.isCanceled = true
    }
}
