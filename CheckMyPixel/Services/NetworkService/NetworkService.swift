//
//  NetworkService.swift
//  CheckMyPixel
//
//  Created by Gavrysh on 2/20/18.
//  Copyright © 2018 Stanislav Cherkasov. All rights reserved.
//

import Foundation

// TODO: REFACTOR TO DOWNLOAD TASK

public enum NetworkServiceError: Error {
    case failed
}

public protocol NetworkService {
    func data(at url: URL, completion: @escaping (Data?, Error?) -> ()) -> NetworkTask
}
