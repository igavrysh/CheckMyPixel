//
//  NetworkServiceImpl.swift
//  CheckMyPixel
//
//  Created by Gavrysh on 2/20/18.
//  Copyright © 2018 Stanislav Cherkasov. All rights reserved.
//

import Foundation

public class NetworkServiceImpl: NetworkService {
    
    // MARK: -
    // MARK: Properties
    
    private let session: URLSession
    
    // MARK: -
    // MARK: Init and Deinit
    
    public init(session: URLSession) {
        self.session = session
    }
    
    // MARK: -
    // MARK: Public
    
    public func data(at url: URL, completion: @escaping (Data?, Error?) -> ()) -> NetworkTask {
        let dataTask = self.defaultSession.dataTask(with: url) { data, response, error in
            DispatchQueue.global(qos: .background).async {
                switch (data, error) {
                case (nil, nil): completion(nil, NetworkServiceError.failed)
                default: completion(data, error)
                }
            }
        }
        
        dataTask.resume()
        
        return NetworkTask(urlSessionTask: dataTask)
    }
    
    let defaultSession = URLSession(configuration: .ephemeral)
    
    // MARK: -
    // MARK: Public
}
