//
//  User.swift
//  CheckMyPixel
//
//  Created by Gavrysh on 2/20/18.
//  Copyright © 2018 Stanislav Cherkasov. All rights reserved.
//

import UIKit

class User {
    var id: String?
    var name: String?
    var image: UserImageData?
    
    init() {
    }
    
}

class UserImageData {
    var urlData: UserImage?
}

class UserImage {
    var url: String?
    
    convenience init(url: String) {
        self.init()
        self.url = url
    }
}
