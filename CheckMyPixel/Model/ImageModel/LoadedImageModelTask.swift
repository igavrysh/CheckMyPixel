//
//  LoadedImageModelTask.swift
//  CheckMyPixel
//
//  Created by Gavrysh on 2/20/18.
//  Copyright © 2018 Stanislav Cherkasov. All rights reserved.
//

import Foundation

struct LoadedImageModelTask: Cancelable {
    
    // MARK: -
    // MARK: Properties
    
    public let isCanceled: Bool = false
    
    // MARK: -
    // MARK: Public
    
    public func cancel() {
        
    }
}
