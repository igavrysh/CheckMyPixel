//
//  ImageModel.swift
//  CheckMyPixel
//
//  Created by Gavrysh on 2/20/18.
//  Copyright © 2018 Stanislav Cherkasov. All rights reserved.
//

import Foundation
import UIKit

public protocol ImageModel {
    var url: URL { get }
    var image: UIImage? { get }
    
    func load(completion: @escaping (UIImage?) -> ()) -> Cancelable
}

public func == (lhs: ImageModel, rhs: ImageModel) -> Bool {
    return lhs.url == rhs.url
}

