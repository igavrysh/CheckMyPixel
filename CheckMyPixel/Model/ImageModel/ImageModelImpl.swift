//
//  ImageModelImpl.swift
//  CheckMyPixel
//
//  Created by Gavrysh on 2/20/18.
//  Copyright © 2018 Stanislav Cherkasov. All rights reserved.
//

import Foundation
import UIKit

public class ImageModelImpl : ImageModel {
    
    // MARK: -
    // MARK: Properties
    public let url: URL
    public private(set) var image: UIImage?
    public let imageLoadService: ImageLoadService
    
    public var isLoaded: Bool {
        return self.image != nil
    }
    
    private let lock = NSRecursiveLock()
    private var isLoading = false
    private var cancelable: Cancelable = LoadedImageModelTask()
    
    // MARK: -
    // MARK: Init and Deinit
    
    init(with url: URL, imageLoadService: ImageLoadService) {
        self.url = url
        self.imageLoadService = imageLoadService
    }
    
    // MARK: -
    // MARK: Public
    
    public func load(completion: @escaping (UIImage?) -> ()) -> Cancelable {
        return self.lock.do {
            if self.isLoaded {
                completion(self.image)
                
                return LoadedImageModelTask()
            }
            
            if self.isLoading {
                return self.cancelable
            } else {
                self.cancelable = self.imageLoadService.fetchImage(url: self.url) { [weak self] image in
                    self?.isLoading = false
                    self?.image = image
                    completion(image)
                }
                
                return self.cancelable
            }
        }
    }
}
