//
//  Tuples.swift
//  CheckMyPixel
//
//  Created by Gavrysh on 2/22/18.
//  Copyright © 2018 Stanislav Cherkasov. All rights reserved.
//

import Foundation

func lift<A, B>(_ tuple: (A?, B?)) -> (A, B)? {
    return tuple.0.flatMap { a in
        tuple.1.map { (a, $0) }
    }
}

public func lift<A, B>(_ a: A?, _ b: B?) -> (A, B)? {
    return lift((a, b))
}

